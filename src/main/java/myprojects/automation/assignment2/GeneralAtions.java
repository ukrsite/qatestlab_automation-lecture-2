package myprojects.automation.assignment2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static myprojects.automation.assignment2.utils.Properties.getBaseAdminUrl;

public class GeneralAtions {
    private WebDriver driver;

    public  GeneralAtions(WebDriver driver) {
        this.driver = driver;
    }

    public void  executeLogin(){
        // get url
        driver.get(getBaseAdminUrl());
        // find element
        WebElement emailElement = driver.findElement(By.id("email"));
        // send keys to element
        emailElement.sendKeys("webinar.test@gmail.com");

        WebElement passwdElement = driver.findElement(By.id("passwd"));
        passwdElement.sendKeys("Xcg7299bnSmMuRLp9ITw");

        WebElement submitLoginElement = driver.findElement(By.name("submitLogin"));
        submitLoginElement.click();
    }

    public void executeLogout() {
        // find the element
        WebElement employee_infos_Element = driver.findElement(By.id("employee_infos"));
        employee_infos_Element.click();
        // click the element
        WebElement header_logout_Element = driver.findElement(By.id("header_logout"));
        header_logout_Element.click();
    }

    public void printTitle(){
        System.out.println("Current windows title : " + driver.getTitle());
    }

    public void checkMainMenuItems() {
        By locator = By.className("maintab");
        List<WebElement> maintabElement =  driver.findElements(locator);
        // get main menu size
        int intItem = maintabElement.size();
        // get url of main menu
        String urlItem = driver.getCurrentUrl();
        // check menu items
        for(int i=0; i<intItem; i++) {
            // renew main menu list
            maintabElement =  driver.findElements(locator);
            // check next menu item
            maintabElement.get(i).click();
            // print page title
            System.out.println("The page title is : " + driver.getTitle());
            // get menu item url
            String titleBeforeRefresh = driver.findElement(By.cssSelector("h2[class]")).getText();
            driver.navigate().refresh();
            String titleAfterRefresh = driver.findElement(By.cssSelector("h2[class]")).getText();
            // check menu item
            if (titleBeforeRefresh.equals(titleAfterRefresh)) {
                System.out.println("The page title is not changed after refresh");
            } else {
                System.out.println("The page title is changed after refresh");
            }
            // back to dashboard
            driver.get(urlItem);
        }
    }
}
