package myprojects.automation.assignment2.tests;

import myprojects.automation.assignment2.BaseScript;
import myprojects.automation.assignment2.GeneralAtions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static myprojects.automation.assignment2.utils.Properties.getBaseAdminUrl;

public class LoginTest extends BaseScript {

    private static WebDriver driver;

    public static void main(String[] args)  {
        // TODO Script to execute login and logout steps
        driver = getConfiguredDriver(getDriver());
        GeneralAtions generalAtions =new GeneralAtions(driver);

        generalAtions.executeLogin();
        generalAtions.executeLogout();
        generalAtions.printTitle();

        driver.quit();
    }

}
